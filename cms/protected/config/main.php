<?php
/**
 * 系统配置参数
 */
return array(
    'basePath'   => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name'       => 'CxrCMS',
    'language'   => 'zh_cn',
    'theme'      => 'default',
    'timeZone'   => 'Asia/Shanghai',
    'preload'    => array('log'),
    'import'     => array(
        'application.models.*',
        'application.components.*',
        'application.extensions.*',
    ),
    'modules'    => array( //模块
        'admini'  => array(
            'class' => 'application.modules.admini.AdminiModule',
        ),
        'account' => array(
            'class' => 'application.modules.account.AccountModule',
        ),
        'gii'     => array(
            'class'     => 'system.gii.GiiModule',
            'password'  => 'root',
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
    ),
    'components' => array(
    	//缓存方式为文件缓存，缓存文件存放在/runtime/cache
        'cache'        => array(
            'class' => 'CFileCache',
        ),
        'db'           => array(
            'connectionString'   => 'mysql:host=127.0.0.1;dbname=cxr_cms',
            'emulatePrepare'     => true,
            'enableParamLogging' => true,
            'enableProfiling'    => true,
            'username'           => 'root',
            'password'           => '123456',
            'charset'            => 'utf8',
            'tablePrefix'        => 'cms_',
        ),
        'errorHandler' => array(
            'errorAction' => 'error/index',
        ),
        //路由
        'urlManager'   => array(
            'urlFormat'      => 'path',
            'urlSuffix'      => '.html',
            'showScriptName' => true,
            'rules'          => array(
                'post/<id:\d+>/*'                  => 'post/show',
                'post/<id:\d+>_<title:\w+>/*'      => 'post/show',
                'post/catalog/<catalog:[\w-_]+>/*' => 'post/index',
                'page/show/<name:\w+>/*'           => 'page/show',
                'special/show/<name:[\w-_]+>/*'    => 'special/show',
                '<controller:\w+>/<action:\w+>'    => '<controller>/<action>',
            ),
        ),
        //框架调试功能
        'log'          => array(
            'class'  => 'CLogRouter',
            'routes' => array(
                array(
                    'class'  => 'CProfileLogRoute',
                    'levels' => 'trace,info,error,warning',
                ),
            ),

        ),
    ),
    'params'     => require (dirname(__FILE__) . DS . 'params.php'),
);
