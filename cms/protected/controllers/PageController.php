<?php
/**
 * 单页控制器
 * @author   cxr
 */
class PageController extends XFrontBase
{
    /**
     * 浏览
     */
    public function actionShow($name)
    {
        $cxrcmsPageModel = Page::model()->find'title_alias=:titleAlias',
            array('titleAlias' => CHtml::encode(strip_tags($name))));
        if (false == $cxrcmsPageModel) {
            throw new CHttpExceptio('status':404, '内容不存在');
        }

        $this->_seoTitle = empty($cxrcmsPageModel->seo_title) ?
            $cxrcmsPageModel->title . ' - ' . $this->_conf['site_name'] :
            $cxrcmsPageModel->seo_title;
        $tpl             = empty($cxrcmsPageModel->template) ? 'show' : $cxrcmsPageModel->template;
        $this->render($tpl, array('cxrcmsPage' => $cxrcmsPageModel));
    }
public function run($actionID)
{
    parent::run($actionID); // TODO: Change the autogenerated stub
}
}
