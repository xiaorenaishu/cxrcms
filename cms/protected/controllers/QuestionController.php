<?php
/**
 * 问答模块控制器
 * @author   cxr
 */
class QuestionController extends XFrontBase
{
    /**
     * 首页
     */
    public function actionIndex()
    {

        $cxrcmsQuestionModel               = new Question();
        $cxrcmsQuestionCriteria            = new CDbCriteria();
        $cxrcmsQuestionCriteria->condition = 'status_is=:status';
        $cxrcmsQuestionCriteria->params    = array('status' => 'Y');
        $cxrcmsQuestionCriteria->order     = 't.id DESC';
        $cxrcmsQuestionCount               = $cxrcmsQuestionModel->count($cxrcmsQuestionCriteria);
        $cxrcmsQuestionPages               = new CPagination($cxrcmsQuestionCount);
        $cxrcmsQuestionPages->pageSize     = 10;
        $cxrcmsQuestionPageParams          = XUtils::buildCondition($_GET, array());
        $cxrcmsQuestionPageParams['#']     = 'list';
        $cxrcmsQuestionPages->params       = is_array($cxrcmsQuestionPageParams) ? $cxrcmsQuestionPageParams : array();
        $cxrcmsQuestionCriteria->limit     = $cxrcmsQuestionPages->pageSize;
        $cxrcmsQuestionCriteria->offset    = $cxrcmsQuestionPages->currentPage * $cxrcmsQuestionPages->pageSize;
        $cxrcmsQuestionList                = $cxrcmsQuestionModel->findAll($cxrcmsQuestionCriteria);
        $this->_seoTitle                   = '留言咨询 - ' . $this->_conf['site_name'];
        $this->render('index', array('cxrcmsQuestionList' => $cxrcmsQuestionList, 'pages' => $cxrcmsQuestionPages));
    }

    /**
     * 提交留言
     */
    public function actionPost()
    {
        if ($_POST['Question']) {
            try {
                $questionModel             = new Question();
                $questionModel->attributes = $_POST['Question'];
                if ($questionModel->save()) {
                    $var['state']   = 'success';
                    $var['message'] = '提交成功';
                } else {
                    throw new Exception(CHtml::errorSummary($questionModel, null, null, array('firstError' => '')));
                }
            } catch (Exception $e) {
                $var['state']   = 'error';
                $var['message'] = $e->getMessage();
            }
        }
        exit(CJSON::encode($var));
    }
}
