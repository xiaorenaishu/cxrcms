<?php
/**
 * @describe 首页控制器
 * @author   cxr
 * @time     2018-01-20
 * @version  [version]
 * @return   [type]
 */
class SiteController extends XFrontBase
{
    /**
     * @describe 首页
     * @author   cxr
     * @time     2018-01-20
     * @version  [version]
     * @return   [type]
     */
    public function actionIndex()
    {
    	$this->render('index', array('model' => $model));
    }

}
