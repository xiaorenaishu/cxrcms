<?php
/**
 * Http工具类
 * 
 * @author        shuguang <5565907@qq.com>
 * @copyright     Copyright (c) 2007-2013 cxrsoft. All rights reserved.
 * @link          http://www.cxrcms.com
 * @package       cxrCMS.Tools
 * @license       http://www.cxrcms.com/license
 * @version       v3.1.0
 */
class XHttp{

	/**
	 * 文件下载
	 */
	static function download ($filename, $showname='', $content='',$expire=180){
		Yii::import( 'application.vendors.*' );
        require_once 'Tp/Http.class.php';
        Http::download($filename, $showname, $content,$expire);
	}
}


