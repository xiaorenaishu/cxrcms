<?php
/**
 * 继承CHttpSession，重写get方法 以能获取数组里面的指定索引的值了（暂时不启用）
 */
class WHttpSession extends CHttpSession
{
    public function get($key, $defaultValue = null)
    {
        $see = $defaultValue;
        $key = explode(".", $key);
        if (count($key) >= 2) {
            if (isset($_SESSION[$key[0]])) {
                $see = $_SESSION[$key[0]];
                $i   = 0;
                foreach ($key as $v) {
                    $i++;
                    if ($i == 1) {
                        continue;
                    }

                    $see = $see[$v];
                }
            }

        } else {
            if (isset($_SESSION[$key[0]])) {
                $see = $_SESSION[$key[0]];
            }
        }

// return isset($_SESSION[$key]) ? $_SESSION[$key] : $defaultValue;
        return $see;

    }
}
