<?php
/**
 * 入口文件
 */
error_reporting(E_ERROR | E_WARNING | E_PARSE);//报错级别
define('DS', DIRECTORY_SEPARATOR);//简写路径分割‘/’‘\’为DS
 define('WWWPATH', str_replace(array('\\', '\\\\'), '/', dirname(__FILE__)));//定义根

$framework = dirname(__FILE__) . DS . 'framework' . DS . 'yiilite.php';
$config    = WWWPATH . DS . 'protected' . DS . 'config' . DS . 'main.php';
require_once $framework;
Yii::createWebApplication($config)->run();
?>

